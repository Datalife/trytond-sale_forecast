# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, MatchMixin
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.transaction import Transaction


class SaleForecast(ModelSQL, ModelView, MatchMixin):
    '''Sale Forecast'''
    __name__ = 'sale.forecast'

    company = fields.Many2One('company.company', 'Company', required=True,
        select=True)
    customer = fields.Many2One('party.party', 'Customer', select=True)
    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date', required=True,
        domain=[('end_date', '>=', Eval('start_date'))],
        depends=['start_date'])
    product = fields.Many2One('product.product', 'Product', required=True)
    quantity = fields.Float('Quantity', required=True,
        digits=(16, Eval('unit_digits', 2)),
        depends=['unit_digits'])
    sold_quantity = fields.Function(fields.Float(
        'Sold quantity', digits=(16, Eval('unit_digits', 2)),
        depends=['unit_digits']), 'get_sold_quantity')
    unit = fields.Function(
        fields.Many2One('product.uom', 'Unit'),
        'get_unit')
    unit_digits = fields.Function(
        fields.Integer('Unit digits'), 'get_unit_digits')
    fulfillment = fields.Function(fields.Float('Fulfillment',
        digits=(16, 4)), 'get_fulfillment')

    def get_rec_name(self, name=None):
        Lang = Pool().get('ir.lang')
        lang = Lang.get()
        values = [
            self.product.name,
            '@ >=%s,<=%s' % (
                lang.strftime(self.start_date),
                lang.strftime(self.end_date))
        ]
        if self.customer:
            values.append('[%s]' % self.customer.name)
        return ' '.join(values)

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    def get_unit(self, name=None):
        if self.product:
            return self.product.default_uom.id
        return None

    def get_unit_digits(self, name=None):
        if self.product:
            return self.product.default_uom.digits
        return 2

    def _get_sales_domain(self):
        domain = [
            ('type', '=', 'line'),
            ('product', '=', self.product),
            ('sale.company', '=', self.company),
            ('sale.state', 'not in', ['draft', 'cancel']),
            ('sale.sale_date', '<=', self.end_date),
            ('sale.sale_date', '>=', self.start_date)]
        if self.customer:
            domain.append(['OR', [
                ('sale.party', '=', self.customer.id),
                ('sale.shipment_party', '=', None)], [
                ('sale.shipment_party', '=', self.customer.id)]
            ])
        return domain

    def get_sold_quantity(self, name=None):
        pool = Pool()
        SaleLine = pool.get('sale.line')

        sale_lines = SaleLine.search(self._get_sales_domain())

        qty = 0
        for line in sale_lines:
            qty += self._get_sold_quantity(line)
        return qty

    def _get_sold_quantity(self, line):
        return self.product.default_uom.compute_qty(line.unit,
            line.quantity, self.product.default_uom)

    @classmethod
    def _get_fulfillment(cls, target, qty):
        if target and qty:
            return round((qty / target), cls.fulfillment.digits[1])
        return float(0)

    def get_fulfillment(self, name=None):
        return self._get_fulfillment(self.quantity, self.sold_quantity)
