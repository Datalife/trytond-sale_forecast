# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale_forecast


def register():
    Pool.register(
        sale_forecast.SaleForecast,
        module='sale_forecast', type_='model')
