======================
Sale Forecast Scenario
======================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.sale_forecast.tests.tools import create_sale_forecast
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company

Install sale_forecast::

    >>> config = activate_modules('sale_forecast')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> address = customer.addresses.new()
    >>> address.name = 'Address 2'
    >>> customer.save()

Create Sale Forecast::

    >>> sale_forecast = create_sale_forecast(company, customer)
    >>> sale_forecast.quantity
    10.0
    >>> sale_forecast.sold_quantity
    5.0
    >>> sale_forecast.fulfillment
    0.5